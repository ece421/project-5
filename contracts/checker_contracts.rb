gem 'test-unit'
require 'test/unit'

module CheckerContracts
  include Test::Unit::Assertions

  # TODO: is there really any difference for preconditions here? Or should they all just
  # be some checker_preconditions(board, pattern)

  def board_conditions(board)
    assert board != nil, "Can't have a nil board"
    assert board.col_size > 0, "Can't have a small board"
    assert board.row_size > 0, "Can't have a small board"
    assert board.grid != nil, "Can't have a nil grid"
    assert board.col_size == board.grid.size, "Column size isn't correct"
    assert board.row_size == board.grid[0].size, "Row size isn't correct"
  end

  def pattern_conditions(pattern)
    assert pattern.is_a?(Regexp)
  end

  def check_preconditions(board, pattern)
    board_conditions(board)
    pattern_conditions(pattern)
  end

  def horizontal_win_preconditions(board, pattern)
    board_conditions(board)
    pattern_conditions(pattern)
  end

  def vertical_win_preconditions(board, pattern)
    board_conditions(board)
    pattern_conditions(pattern)
  end

  def diagonal_win_preconditions(board, pattern)
    board_conditions(board)
    pattern_conditions(pattern)
  end

  def board_full_preconditions(board)
    board_conditions(board)
  end

end