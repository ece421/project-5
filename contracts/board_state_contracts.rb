
gem 'test-unit'
require 'test/unit'

module BoardStateContracts
  include Test::Unit::Assertions

  def initialize_preconditions(players, board)
    assert players != nil, "No nil players"
    assert board != nil, "No board?"
  end

end