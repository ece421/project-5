
gem 'test-unit'
require 'test/unit'

module ControllerContracts
	include Test::Unit::Assertions

	def set_signal_handlers_preconditions
    assert @builder != nil, "Can't have a nil builder"
	end

  def set_starting_handlers_preconditions
    assert @builder != nil, "Can't have nil builder"
  end

  def set_game_handlers_preconditions
    assert @builder != nil, "No nil builder please"
  end

  def drop_on_col_preconditions(col)
    assert @builder != nil, "Builder can't be nil"
    assert @game != nil, "Game can't be nil"
    assert col >=0, "Column must be larger than 0"
    assert col < @game.board.col_size, "Column must be smaller than size of board"
  end

  def update_status_label_preconditions
    assert @builder != nil, "Builder can't be nil"
  end

  def starting_menu_ok_preconditions
    assert @builder != nil, "Builder can't be nil"
  end

  def starting_menu_ok_postconditions
    assert @builder != nil, "Builder can't be nil"
    # @game could be nil if user didn't enter good name
  end

end