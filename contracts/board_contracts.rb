gem 'test-unit'
require 'test/unit'

module BoardContracts
  include Test::Unit::Assertions

  def class_invariant
    assert @grid != nil, "Grid can't be nil"
    assert @col_size > 0 && @row_size > 0, "Grid was too small"
    assert_equal @grid.size, @col_size, "Column size wasn't correct"
    assert_equal @grid[0].size, @row_size, "Row size was incorrect"
  end

  def initialize_preconditions(row, col)
    assert row > 0, "Row too small"
    assert col > 0, "Col too small"
  end

  def initialize_postconditions(row, col)
    assert_equal @row_size, row, "Row size is incorrect"
    assert_equal @col_size, col, "Column size is incorrect"
  end

  def column_full_preconditions(col)
    assert col >= 0, "Col was too small"
    assert col < @col_size, "Col doesn't fit in board"
  end

  def get_diagonals_postconditions(diagonals)
    assert diagonals.is_a?(Array), "Diagonals isn't an array"
  end

  def add_piece_preconditions(col, type)
    assert col >= 0, "Column too small"
    assert col < @col_size, "Column too big"
  end

  def get_current_row_of_col_preconditions(col)
    assert col >=0, "Column too small"
    assert col < @col_size, "Column too big"
  end

end