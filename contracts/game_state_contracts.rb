
gem 'test-unit'
require 'test/unit'

module GameStateContracts
	include Test::Unit::Assertions

	def check_game_state_preconditions(board)
		assert board!=nil
	end

	def class_invariant(board,players,turns)
		assert board!=nil, "The state of a game has to have a board...right"
		assert players >= 2 , "How can there be so few players in a game!"
		assert turns >= 0  , "So how do we play a game backwards?!"
	end

end