
gem 'test-unit'
require 'test/unit'

module DatabaseContracts
	include Test::Unit::Assertions

	def createGame_preconditions(game,players,board )
    assert game >=0, "Incorrect game type"
    assert game == 1, "Incorrect game type"
    assert players.length >= 2 , "No one plays with themself, they atleast have a virtual buddy!"
    players.each {|player| 
      assert player.name != nil
      assert player.type != nil}
    assert board !=nil 
  end

  def initialize_postconditions(server)
    assert server != nil
  end

  def addPlayerToGame_preconditions(game,player)
    assert gameInDB(game)
    assert player.name != nil
    assert player.type != nil
  end

  def gameInDB_precondtitions(game)
    assert game!=nil
    assert game.isNumeric?
  end

  def getPlayerStats_preconditions(player)
    assert playerInDB?(player)
  end

  def addPlayerToServer_preconditions(player)
    assert player != nil
    assert player.name !=nil
  end

  def addGame_preconditions(game,gameID,board)
    assert game!=nil
    assert !gameNotInDB(gameID)
    assert board != nil
  end

  def saveGame_preconditions(game,gameID,board)
    assert game!=nil
    assert @games.contains(gameID)
    assert board != nil
  end

  def loadGane_precondtions(gameID)
    assert game!=nil
  end
end