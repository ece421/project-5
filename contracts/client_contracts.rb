
gem 'test-unit'
require 'test/unit'

module ClientContracts
	include Test::Unit::Assertions

	def client_connect_postconditions(host )
    assert @ClientServer !=nil
    assert @Host!=nil
	end

  def connect_preconditions(ip,port)
    assert ip != nil
    assert port != nil
  end

  def createGame_preconditions(game,players )
    assert game >=0, "Incorrect game type"
    assert game <=1, "Incorrect game type"
    assert players.length >= 2 , "No one plays with themself, they atleast have a virtual buddy!"
  end


end