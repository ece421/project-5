gem 'test-unit'
require 'test/unit'

require_relative '../checker/checker'
require_relative '../model/player/player'

module GameContracts
	include Test::Unit::Assertions

	def initialize_preconditions(rows, cols, pattern, p1name, p1color, p2name, p2color, multiplayer_game)
    assert rows > 0, "Need more than 0 rows"
    assert cols > 0, "Need more than 0 cols"
    assert pattern == 0 || pattern == 1, "Invalid pattern"
    assert p1name != nil, "Don't want a nil name"
    assert p1color == 'b' || p1color == 'e' || p1color == 'g' || p1color == 'O' || p1color == 'r' ||
        p1color == 'T', "Invalid color"
    assert p2name != nil, "No Nil names"
    assert p2color == 'b' || p2color == 'e' || p2color == 'g' || p2color == 'O' || p2color == 'r' ||
               p2color == 'T', "Invalid color"
    assert multiplayer_game == 0 || multiplayer_game == 1 ||
        multiplayer_game == 2 || multiplayer_game == 87, "Invalid game type"
  end

  def class_invariant
    assert @board != nil, "NO nil game"
    assert @pattern == Checker::PATTERN_CONNECT_FOUR || @pattern == Checker::PATTERN_OTTO_TOOT, "Invalid pattern"
    assert @gamePlayers.size == 2, "Not two players in player array"
    assert @gamePlayers[0].is_a?(Player), "Thing in gameplayer box is not a player"
    assert @gamePlayers[1].is_a?(Player), "Thing in gameplayer box is not a player"
    assert @view != nil
  end

  def col_preconditions(col)
    assert col >=0, "Col to small"
    assert col < @board.col_size, "Col too big"
  end

  def get_game_pattern_preconditions(p)
    assert p == 1 || p == 0, 'Invalid pattern'
  end

end