
gem 'test-unit'
require 'test/unit'

module ServerContracts
	include Test::Unit::Assertions

	def server_start_postconditions
    assert @database!=nil
    assert @server !=nil
    assert @games != nil
    assert @players != nil
	end

  def createGame_preconditions(game,players )
    assert game >=0, "Incorrect game type"
    assert game ==1, "Incorrect game type"
    assert players.length >= 2 , "No one plays with themself, they atleast have a virtual buddy!" 
  end

  def createGame_postconditions(game,gameID)
    assert game!=nil
    assert gameID!=nil
  end

  def makeMove_preconditions(gameId, player, column)
    assert gameID != nil
    assert player !=nil
    assert column >= 0;
  end

  def loadGame_preconditions(game)
    assert game!=nil
  end

end