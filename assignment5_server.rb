require_relative 'server/server'

# Last project!
# Group 5: Chris Hut, Vincent Phung

# Server file, starts the server, runs until killed

# command line usage: ruby ./assignment5_server.rb

# if you have to/want to/love to change the port here is the place to do it!
port = 50505

Server.new(port)