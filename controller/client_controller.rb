require_relative '../model/game'
require_relative '../contracts/controller_contracts'
require_relative '../utils'
require_relative '../server/response'
require_relative '../checker/checker'
require_relative '../model/gamez'

class ClientController

  include ControllerContracts
	include Utils

	@view = nil
	@game = nil
	@builder = nil

  # Sets the view
	def set_view(view)
		@view = view
	end

  # Sets the builder
	def set_builder(builder)
		@builder = builder
	end

	def set_client(client)
		@myClient = client
	end

  # Sets up signal handlers and window
	def set_signal_handlers()
    set_signal_handlers_preconditions
		setLoginWindowHandlers()
		setStartingHandlers()
		setGameHandlers()
		window = @builder.get_object("window_launcher")
      	window.signal_connect( "destroy" ) { Gtk.main_quit }
		window_game = @builder.get_object("window_game")
		window_game.signal_connect( "destroy" ) { Gtk.main_quit }
		setDialog()
		setButtons()
		@otherDialog = @builder.get_object("dialogbox1")
		@otherDialog.signal_connect("destroy"){Gtk.main_quit}
	end

	# sets top buttons
	def setButtons
		menuButton = @builder.get_object("menuButton")
      	menuButton.signal_connect( "activate" ) { mainMenuAndRefresh}
      	refreshButton = @builder.get_object("refreshButton")
      	refreshButton.signal_connect("activate"){refresh_board_click}
      	exitBut = @builder.get_object("exitProgramButton")
      	exitBut.signal_connect("activate"){Gtk.main_quit}
	end

	# refreshes the board
	def refresh_board_click
		response = Response::decode(@myClient.main_server.get_board(@game_id))
		if response.status_code == Response::STATUS_OK
			if @board != response.body
				@board = response.body
				@view.updateBoard(@board)
				status = Checker::check(@board)
				if status != Game::GAME_NOT_OVER
					game_over(status)
				end
			end
		end
	end

	# called when game is over
	def game_over(status)
		if status == Game::GAME_OVER_TIE
			setDialogMsg("Tie Game!")
		elsif status == Game::GAME_OVER_WIN
			winner = Response::decode(@myClient.main_server.get_current_player(@game_id)).body
			winner_name = Response::decode(@myClient.main_server.get_user_name(winner))
			setDialogMsg("#{winner_name} won!")
		end

	end

	def mainMenuAndRefresh()
		refreshGameList
    	@window_launcher = @builder.get_object("window_launcher")
    	@window_launcher.show()
		window_game = @builder.get_object("window_game")
		window_game.hide()
	end

	def setLoginWindowHandlers()
		login_button = @builder.get_object("button_login")
		login_button.signal_connect("clicked"){userLogin}
	end

	def userLogin
		entryText = @builder.get_object("entry_username")
		@username = entryText.text
		@login_window = @builder.get_object("window_login")
		@login_window.hide()
		@window_launcher = @builder.get_object("window_launcher")
    	@window_launcher.show()

    	entryText = @builder.get_object("entry_username")
		@username = entryText.text
		# now we should go and check the thing about username and shit
		encoded = @myClient.main_server.create_user(@username)
		response = Response::decode(encoded)
		if (response.status_code == Response::STATUS_OK)
			@userID = response.body
		else
			encoded = @myClient.main_server.get_user_id(@username)
			response = Response::decode(encoded)
			@userID = response.body unless response.status_code == Response::STATUS_USER_DOES_NOT_EXIST
		end
		@login_window = @builder.get_object("window_login")
		@login_window.hide()
		refreshGameList
		@window_launcher = @builder.get_object("window_launcher")
    @window_launcher.show()
	end

	# Set error_dialog handler
	def setDialog()
		@dialog = @builder.get_object("dialogbox")
		@dialog.signal_connect( "destroy" ) { Gtk.main_quit }
		button_restart = @builder.get_object("button_restart")
		button_restart.signal_connect("clicked"){restartGame}
		button_exit = @builder.get_object("button_exitDialog")
		button_exit.signal_connect("clicked"){Gtk.main_quit}
	end



	# Set leader_board dialog handlers
	def setLeaderDiag()
		@leader_dialog = @builder.get_object("leaderBoards")
		@leader_dialog.signal_connect("destroy"){@leader_dialog.hide()}
		exit_button = @builder.get_object("button_exitLeaderBoards")
		exit_button.signal_connect("clicked"){@leader_dialog.hide()}
	end
	# Handler for restartingGame
	def restartGame()
		clearBoard()
		@dialog.hide()
		window = @builder.get_object("window_launcher")
		window.show()
	end
	
	def clearBoard()
		 path =  File.expand_path("../resources", File.dirname(__FILE__))
		 		(0..41).each {|num|
    			tokenImage = @builder.get_object(num.to_s)
        	tokenImageName = "/empty.png"
        	tokenImage.set_file(path.to_s+tokenImageName)
        }
	end
	
  # Sets up signal handlers
	def setStartingHandlers()
    set_starting_handlers_preconditions
		buttonCreateGame = @builder.get_object("button_create_game")
		buttonRefresh = @builder.get_object("button_refresh")
		buttonJoin = @builder.get_object("button_join")
		buttonLeaderboards = @builder.get_object("button_leaderboards")
		buttonCancel = @builder.get_object("button_cancel")

		buttonCreateGame.signal_connect("clicked"){createGame}
		buttonRefresh.signal_connect("clicked"){refreshGameList}
		buttonJoin.signal_connect("clicked"){joinGame}
		buttonLeaderboards.signal_connect("clicked"){leaderBoardsClick}
		buttonCancel.signal_connect("clicked"){startingMenuCancel}
		# TODO need stuff for leaderboards
		setLeaderDiag()
			#}
		leader_dialog = @builder.get_object("leaderBoards")
		leader_dialog.signal_connect("destroy"){Gtk.main_quit}

	end

  # sets up in game handlers
	def setGameHandlers()
    set_game_handlers_preconditions
		(1..7).each{ |col|
			buttonDropPiece = @builder.get_object("button_dropCol"+col.to_s)
			buttonDropPiece.signal_connect("clicked"){dropOnCol(col)}
		}
	end

  # Checks if someone has won
	def checkWin()
		@game.checkState
	end

  # Tries to drop a piece in the column
  # will need to be modified for online/offline type games
  # so prob wrap all this stuffs with an if/else if lazy
	def dropOnCol(col)
		refresh_board_click

	if @game == nil
		dropColOnline(col)
	elsif @game.isOffline?
	    label = @builder.get_object('label_currentStatus')
		if @game.colFull?(col-1)
	      label.set_text('Column is full, try another')
	    else
	      	label.set_text("#{@game.currentPlayer.name}'s turn")
			imageNum = @game.getIndexOf(col-1)
			@state = @game.checkState()
				if @state == Game::GAME_NOT_OVER
	        		drop_on_col_preconditions(col-1)
					@game.play_game(col-1)
					path =  File.expand_path("../resources", File.dirname(__FILE__))
					tokenImage = @builder.get_object(imageNum.to_s)
					tokenImage.set_file(path.to_s+getImageName(@game.currentPlayer.type))
	        		@state = @game.checkState
				  	@game.switchPlayer() unless @state != Game::GAME_NOT_OVER
	        		updateStatusLabel(@game.currentPlayer)
				elsif @state == Game::GAME_OVER_WIN
					label.set_text(@game.currentPlayer.name+ " has won")
					setDialogMsg(@game.currentPlayer.name+ " has won")
					showDiag()
				elsif @state == Game::GAME_OVER_TIE
					label.set_text("It's a draw, everyone loses (or wins)")
					setDialogMsg("It's a draw, everyone loses (or wins)")
					showDiag()
				end
			end
			@state = @game.checkState()
			if @state == Game::GAME_OVER_WIN
				label.set_text(@game.currentPlayer.name+ " has won")
				setDialogMsg(@game.currentPlayer.name+ " has won")
				showDiag()
			elsif @state == Game::GAME_OVER_TIE
				label.set_text("ITS A TIE!")
				setDialogMsg("ITS A TIE!")
				showDiag()
			end
		end
	end

	def dropColOnline(col)
		return if @game_id == -1 # no game being player

		label = @builder.get_object('label_currentStatus')

		if @board.column_full?(col-1)
			#column already full
			label.set_text("Column: #{col} is full, try another")
			return
		end

		my_turn_response = Response::decode(@myClient.main_server.is_it_my_turn(@game_id, @userID))
		if my_turn_response.status_code != Response::STATUS_OK || !my_turn_response.body
			label.set_text('Not your turn!')
			return
		end

		type = Response::decode(@myClient.main_server.get_current_type(@game_id)).body

		add_response = Response::decode(@myClient.main_server.add_piece(@game_id, col-1, type, @userID))
		if add_response.status_code == Response::STATUS_OK
				refresh_board_click
			end
		end

	#makes diag appear
	def showDiag()
		@dialog.show()
	end

	def showOtherDiag()
		@otherDialog.show()
	end


	# locks down the buttons so user cant press on dem
	# TODO make sure this actually works know what im saying
	def lockDownbuttons()
		(1..7).each{ |col|
			buttonDropPiece = @builder.get_object("button_dropCol"+col.to_s)
			buttonDropPiece.sensitive = false;
		}
	end

	# re enables mah buttons
	# TODO make sure this actually works like a dawg
	def lockDownbuttons()
		(1..7).each{ |col|
			buttonDropPiece = @builder.get_object("button_dropCol"+col.to_s)
			buttonDropPiece.sensitive = true;
		}
	end

	# sets the message label in dialog box
	def setDialogMsg(t)
		label  = @builder.get_object('label_dialog')
		label.set_text(t)
		showDiag
	end

	def setOtherDialog(s)
		label  = @builder.get_object('label_dialog1')
		label.set_text(t)
	end

  # Ends game if cancel was chosen
	def startingMenuCancel
		Gtk.main_quit
	end

  # Updates status label for current player
	def updateStatusLabel(currentPlayer)
    	update_status_label_preconditions
		label = @builder.get_object("label_currentStatus")
		status = " doing something!"
		label.set_text(currentPlayer.name.to_s+"'s turn")
	end 

  # Sets up new game on OK click
  # TODO need to account for multiplayer/ computer now!
  #  |-> if lazy no more local hahah 
	def createGame
		starting_menu_ok_preconditions
		radiobutton_c4 = @builder.get_object("radiobutton_c4")
		radiobutton_otto = @builder.get_object("radiobutton_otto")
		radiobutton_human = @builder.get_object("radiobutton_human")
		radiobutton_ai = @builder.get_object("radiobutton_ai")
		radiobutton_dummy = @builder.get_object("radiobutton_dummy")
		radiobutton_smarty = @builder.get_object("radiobutton_smarty")

		text_box_player_2 = @builder.get_object('player_2_name')

		# TODO check if we actually need this and which server ip/port would
		# this actually be specifiying
		text_box_ip = @builder.get_object("ip_entry")
		text_box_port = @builder.get_object("port_entry")


		#start logic
		gameType = -1
		if radiobutton_c4.active?
			gameType = 0
			player_1_token = 'b'
			player_2_token = 'r'
		else
			gameType = 1

			player_1_token = 'O'
			player_2_token = 'T'
		end
		player_1_name = @username
		player_2_name = text_box_player_2.text

		if player_1_name.empty?
			player_1_name = 'Player 1'
		end

		if player_2_name.empty?
			player_2_name = 'Player 2'
		end

		status = @builder.get_object('label_currentStatus')
		status.text = "#{player_1_name}'s turn"

		# TODO change these not longer singleplayer/multiplayer
		# change to all references to Game::SINGLEPLAYER to Game::ONLINEPLAYER
		# or something liek that
		@multiplayer = Game::SINGLEPLAYER
		if radiobutton_ai.active?
			@multiplayer = Game::MULTIPLAYER_SMART
			if radiobutton_dummy.active?
				@multiplayer = Game::MULTIPLAYER_DUMB
			end
		end

		if (@multiplayer!=Game::SINGLEPLAYER)
			@game = Game.new(6, 7, gameType, player_1_name, player_1_token, player_2_name, player_2_token, @multiplayer, -1)
			hideMenuOpenGame()
		else

			# TODO makes sure this is correct
			user_id_response = Response::decode(@myClient.main_server.get_user_id(player_2_name))
			if user_id_response.status_code == Response::STATUS_OK
				player_2_user_id = user_id_response.body
			else
				player_2_user_id = -1
			end

			response = Response::decode(@myClient.main_server.start_game(player_2_user_id, @userID, player_1_token, player_2_token, gameType))
			if (response.status_code == Response::STATUS_OK)
				@game_id = response.body
				hideMenuOpenGame()

				# CREATE NEW BOARD HERE
				game_id = response.body

				@board = Response::decode(@myClient.main_server.get_board(game_id)).body

				@window_launcher = @builder.get_object("window_launcher")
				@window_launcher.hide()
				window_game = @builder.get_object("window_game")
				window_game.show()

			elsif response.status_code == Response::STATUS_GAME_ALREADY_IN_PROGRESS
				setDialogMsg("Active Game with #{player_2_name} already exists");
				# TODO make sure this dialog doesnt close the whole game after clicking whatever!
			elsif response.status_code == Response::STATUS_USER_DOES_NOT_EXIST
				setDialogMsg("User: #{player_2_name} does not exist")
			end
		end
		@game.setView(@view) unless @game == nil
		starting_menu_ok_postconditions
	end

	def hideMenuOpenGame
		window_game = @builder.get_object("window_game")
		window_game.show()
		window = @builder.get_object("window_launcher")
		window.hide()
	end

	# refreshes saved games on the server that we can join
	def refreshGameList
		# TODO implement
		# so something like this:
		response = Response::decode(@myClient.main_server.get_current_games(@userID))
		if response == Response::STATUS_USER_DOES_NOT_EXIST
			# TODO throw error message or dialog or w/e user doesnt exist
				setOtherDialog("Warning: Something went wrong you do not have a valid userID!")
				showOtherDialog()
		else
			gamelist = @builder.get_object("list_my_games")
			gamelist.clear
			@myCurrentGames= response.body
			@myCurrentGames.each{|element|
				row = gamelist.append
				boarding = element[3]
				row.set_value(0, element[0]) #gameid
				if element[3].pattern == Checker::PATTERN_CONNECT_FOUR
					row.set_value(1,"C4") #type
				else
					row.set_value(1,"OTTO")
				end
				response = Response::decode(@myClient.main_server.get_user_name(element[1]))
				row.set_value(2,response.body) #who's turn
				if (element[2] == @userID) 
					response = Response::decode(@myClient.main_server.get_user_name(element[1]))
					row.set_value(3,response.body) #opponent
				else
					response = Response::decode(@myClient.main_server.get_user_name(element[2]))
					row.set_value(3,response.body) # opponent
				end
		}
		end	
	end

	# if we have a timer/thread/GLib::Timeout run this every x seconds
	def refreshGame()
		response = Marshal.load(@myClient.main_server.is_it_my_turn())
		if (response.status_code == Response.OK)
			if (@game.currentPlayer.name != @username)
				@game.switchPlayer
			end
		else
			if (@game.currentPlayer.name == @username)
				@game.swtichPlayer
			end
		end 
		response = Marshal.load(@myClient.main_server.get_board(@game.game_id))
		if (response == Reponse::STATUS_OK)
			@game.updateBoard(reponse.body)
		end
		@view.update(new BoardState(@game.players, @game.board))
	end

	def joinGame
		# TODO implement
		# // check what the index is for the user selected thing check gtk api
		tree_game_list = @builder.get_object("treeview1")
		selected = tree_game_list.selection.selected
		if selected!=nil
			response = Response::decode(@myClient.main_server.get_board(selected[0]))
			if response.status_code!= Response::STATUS_GAME_DOES_NOT_EXIST
				@board = response.body
				@game_id = selected[0]
				@view.updateBoard(@board)
				@window_launcher = @builder.get_object("window_launcher")
    			@window_launcher.hide()
				window_game = @builder.get_object("window_game")
				window_game.show()
			end
			# TODO reset the board
			# TODO see what is done in creating new game change to comply
			# TODO do we need to set board/game images
			# board state is just a game have to change this shit
			# @view.refresh(@boardState)
			# also maybe start a timer here for x amount of seconds
			# aka almost a thread lol -> that get_game from server
			# and then update board and status and buttons
			# THIS MIGHT BE USEFUL - basically GTK2 provided tool 
			# that runs that function every x seconds!
			#    GLib::Timeout.add(500) {
      		#		just gotta update that board there! so prob
      		# 			calling get_board , is_it_my_turn(game_id)
      		# 			is calling the update() method in @view or something 
    		#	}
		end
		

	end


	# implements loading data to leaderboard dialog when clicked
	def leaderBoardsClick
		leaderBoard_tree = @builder.get_object("list_leaderBoards")
		leaderBoard_tree.clear
		response = Response::decode(@myClient.main_server.get_leaderboard())
		if response.status_code == Response::STATUS_OK
			@myLeaderBoards = response.body
			@myLeaderBoards.each{ |element|
				row = leaderBoard_tree.append
				row.set_value(0,element[0])
				row.set_value(1,element[1])
				row.set_value(2,element[2])
				row.set_value(3,element[3])
			}
		end
		leader_dialog = @builder.get_object("leaderBoards")
		leader_dialog.show();

	end


end
