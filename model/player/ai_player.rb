require File.dirname(__FILE__) + '/../../checker/checker'
require_relative 'player'
require_relative '../../contracts/player_contracts'
require_relative '../../model/game'

class AIPlayer < Player

  # constants for Difficulty 
  DUMB = 0 
  SMART = 1

  # Creates new AI player
  def initialize(type, name, board_size, pattern, other_type, difficulty)
    @player_type = Player::AI_PLAYER
    @pattern = pattern
    @other_type = other_type
    @difficulty = difficulty # default to smart opponent for now
    super(type, name, board_size)
    class_invariant
  end

  # Returns AIs move for the board
  def get_column(board)
    board_preconditions(board)
    class_invariant
    if @difficulty == SMART
      col = check_for_win(board)
      if col == -1
        # We can't win, so lets find out where we shouldn't go
        safe_cols = check_for_opponent_win(board)
        if safe_cols.size == 0
          # We lose no matter what
          for col in 0..@size-1
            return col unless board.column_full?(col)
          end
          # If we get here shit is messed up
          return rand(@size-1)
        end

        # TODO: Add more AI, try and make a bigger line of pieces, or maybe try and block all possible wins of opponent
        # two turns from now
        for col in safe_cols
          return col unless board.column_full?(col)
        end
        # if we here something else is messed up
        return rand(@size-1)
      else
        return col
      end
    else

      randCol = rand(board.col_size-1)
        while (board.column_full?(randCol))
          randCol = rand(board.col_size-1)
        end
      return randCol
    end

  end

  # returns an array of that values, any of which, do not allow the opponent to win next turn, could be empty
  def check_for_opponent_win(board)
    board_preconditions(board)
    class_invariant
    safe_values = []
    for col in 0..board.col_size-1
      next if board.column_full?(col)

      copy_board = Marshal.load(Marshal.dump(board))
      copy_board.add_piece(col, @type)

      for opponent_col in 0..board.col_size-1
        next if copy_board.column_full?(opponent_col)

        another_board = Marshal.load(Marshal.dump(copy_board))
        another_board.add_piece(opponent_col, @other_type)

        if Checker.check(another_board) == Game::GAME_OVER_WIN
          # We could lose if we played a piece here, remove it from safe_values
          safe_values.delete(col)
          # Breaks inner loop, acting as continue for outer loop
          break
        end
        safe_values << col
      end
    end

    return safe_values.uniq
  end

  # returns column that will win game for AI player, or -1 no column exists
  def check_for_win(board)
    board_preconditions board
    class_invariant
    for col in 0..@size-1
      next if board.column_full?(col)

      # Note: ruby sucks
      copy_board = Marshal.load(Marshal.dump(board))
      # But actually, no real way of doing a deep copy

      copy_board.add_piece(col, @type)

      if Checker.check(copy_board) == Game::GAME_OVER_WIN
        # We found a winning combo
        return col
      end
    end
    return -1
  end

end