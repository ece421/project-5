require_relative "./player"
require_relative "../../contracts/player_contracts"

# class that represents a human player
class HumanPlayer < Player
	include PlayerContracts
	attr_reader :type, :name


  # Initialized the player_type
  def initialize(type, name, board_size)
    @player_type = Player::HUMAN_PLAYER
    super(type, name, board_size)
    class_invariant
  end

  # Returns the chosen position by human player
  def get_column(board)
    board_preconditions(board)
    drop_on_col_preconditions(col)
    # TODO: This is just cheap get input method, should be moved to controller somewhere eventually
    begin
      a = Integer(gets.chomp[0])
      if a < 0 || a >= @size
        puts "invalid between 0 and #{@size}"
        return get_column(board)
      end
      return a
    rescue ArgumentError
      puts "give me a number, try again"
      return get_column board
    rescue TypeError
      puts "put a number in there"
      return get_column board
    end
  end

end