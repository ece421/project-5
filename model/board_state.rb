class BoardState

	attr_reader :players , :board , :currentPlayer

  # Create new board state
	def initialize(players, board) 
		@players = players
		@board = board
		@currentPlayer = @players[0]
	end
end