require_relative '../contracts/piece_contracts'

class Piece

  include PieceContracts

  attr_reader :type

  # Constant for Empty piece
  EMPTY = ' '

  # Creates a new piece, type should be a single character
  def initialize(type)
    #initialize_preconditions type
    @type = type
    #class_invariant
  end

  # Returns if the piece is an empty piece
  def is_empty?
    class_invariant
    @type == EMPTY
  end

  # String representation of piece
  def to_s
    class_invariant
    @type.to_s
  end

end