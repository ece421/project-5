require File.dirname(__FILE__) + '/../model/piece'
require File.dirname(__FILE__) + '/../contracts/board_contracts'

require_relative '../checker/checker'

class Board

  include BoardContracts

  attr_reader :grid, :col_size, :row_size

  # Creates the grid of pieces, and initializes them full of empty pieces
	def initialize(rows, cols, pattern=Checker::PATTERN_CONNECT_FOUR)
    # TODO: Update pre/post conditions
    #initialize_preconditions(rows, cols)
    @grid = Array.new(cols) {Array.new(rows, Piece.new(Piece::EMPTY))}
    @col_size = cols
    @row_size = rows
    @pattern = pattern
    class_invariant
    #initialize_postconditions(rows, cols)
	end

  def pattern
    @pattern
  end

  # Returns if the specified column is full, by checking if top piece in column is empty
  def column_full?(col)
    column_full_preconditions(col)
    class_invariant
    !@grid[col][@row_size-1].is_empty?
  end

  # Returns next row for a column
  def getCurrentRowOfCol(col)
    class_invariant
    get_current_row_of_col_preconditions(col)
    row = @grid[col].count{|x| x.type != Piece::EMPTY}
  end

  # returns all of the diagonals of the array
  def get_diagonals
    # From: https://gist.github.com/EvilScott/1755729
    class_invariant
    [@grid, @grid.map(&:reverse)].inject([]) do |all_diags, matrix|
      ((-matrix.count + 1)..matrix.first.count).each do |offet_index|
        diagonal = []
        (matrix.count).times do |row_index|
          col_index = offet_index + row_index
          diagonal << matrix[row_index][col_index] if col_index >= 0
        end
        all_diags << diagonal.compact if diagonal.compact.count > 1
      end
      get_diagonals_postconditions(all_diags)
      class_invariant
      all_diags
    end
  end

    # Adds a piece to a specified column
  def add_piece(col, type)
    add_piece_preconditions(col, type)
    class_invariant
    row = @grid[col].count{|x| x.type != Piece::EMPTY}
    @grid[col][row] = Piece.new(type)
    class_invariant
  end

  # Returns true if board is full of pieces
  def full?
    class_invariant
    0.upto(@col_size-1) do |col|
      return false unless column_full?(col)
    end
    return true
  end

  # Overwriting to string
  alias_method :display_board, :to_s

  # displays that board of so fancy
  def display_board
    class_invariant
    (@row_size-1).downto(0) do |row|
      0.upto(@col_size-1) do |col|
        element = @grid[col][row]
        print "|"
        if(element.is_empty?)
          print " "
        else
          print "#{element.type}"
        end
      end
      print '|'
      puts
    end
  end

  # Returns index of column
  def getIndexOf(col)
    return col + getCurrentRowOfCol(col)*@col_size
  end

end