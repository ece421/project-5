require File.dirname(__FILE__) + '/../model/board'
require File.dirname(__FILE__) + '/../checker/checker'
require File.dirname(__FILE__) + '/../model/player/player'
require File.dirname(__FILE__) + '/../model/player/ai_player'
require File.dirname(__FILE__) + '/../model/player/human_player'
require File.dirname(__FILE__) + '/../contracts/game_contracts'
require File.dirname(__FILE__) + '/../model/board_state'

class Game
  
  include GameContracts
  attr_reader :board 

  # Constants for status of game
  GAME_OVER_WIN = 1
  GAME_NOT_OVER = -2
  GAME_OVER_TIE = -3
  GAME_OVER_QUIT = -4

  # Constants for type of game
  MULTIPLAYER_DUMB = 0
  MULTIPLAYER_SMART = 1
  SINGLEPLAYER = 2
  MULTIPLAYER_ONLINE = 87

  # TODO gotta change stuff probally for ONLINE
  # some of this shit doesnt apply
  # Creates a new game
  # TODO added game_id prob gonna need to check for pre/post 2
  def initialize(rows, cols, pattern, p1name, p1color, p2name, p2color, multiplayer_game, game_id)
    initialize_preconditions(rows, cols, pattern, p1name, p1color, p2name, p2color, multiplayer_game)
    @pattern = get_game_pattern(pattern)
    @board = Board.new(rows, cols, pattern)

    player_1 = HumanPlayer.new(p1color, p1name, @board.col_size)
    if multiplayer_game == SINGLEPLAYER
      player_2 = HumanPlayer.new(p2color, p2name, @board.col_size)
    else
      if multiplayer_game == MULTIPLAYER_DUMB
        difficulty = AIPlayer::DUMB
      else
        difficulty = AIPlayer::SMART
      end

      player_2 = AIPlayer.new(p2color, p2name, @board.col_size, @pattern, p1color, difficulty)

    end
    @gamePlayers = Array.new
    @gamePlayers.push(player_1)
    @gamePlayers.push(player_2)
  end

  # Sets the view
  def setView(view)
    @view = view
    class_invariant
  end

  # Returns the state
  def checkState
    class_invariant
    Checker.check(@board)
  end

  # Places a piece in column
  def play_game(col)
    col_preconditions(col)
    class_invariant
    @board.add_piece(col, currentPlayer().type)
    class_invariant
  end

  # Returns if the column is full
  def colFull?(col)
    col_preconditions(col)
    class_invariant
    @board.column_full?(col)
  end

  # returns the current player
  def currentPlayer
    class_invariant
    @gamePlayers[0]
  end

  # Switches current player
  def switchPlayer()
    class_invariant
    @gamePlayers.reverse!
    cPlayer = currentPlayer
    if cPlayer.instance_of?(AIPlayer)
      return if checkState != GAME_NOT_OVER
      notifyView(BoardState.new(@gamePlayers,@board))
      aiMove = cPlayer.get_column(@board)
      play_game(aiMove)
      notifyView(BoardState.new(@gamePlayers,@board))
      switchPlayer unless checkState != GAME_NOT_OVER
    end
    class_invariant
  end

  # Update view regarding board state
  def notifyView(boardstate)
    class_invariant
    @view.update(boardstate)
    class_invariant
  end

  # Returns index of column
  def getIndexOf(col)
    class_invariant
    col_preconditions(col)
    row = @board.getCurrentRowOfCol(col)
    return col + row*@board.col_size
  end

    # Returns pattern of game chosen by user
  def get_game_pattern(p)
    get_game_pattern_preconditions(p)
    puts "pattern: #{p}"
    if p == 'connect_4'
      return Checker::PATTERN_CONNECT_FOUR
    else
      return Checker::PATTERN_OTTO_TOOT
    end
  end
end