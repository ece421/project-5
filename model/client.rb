require "xmlrpc/client"
require File.dirname(__FILE__) + '/../contracts/client_contracts'

# this is the client object
# its kind of like the server in that the server can call it !
# remember the customer is always right! 
class Client

  # include ClientContracts
  attr_reader :main_server
  # TODO add other methods duh!

  def initialize(ip,port)
    # TODO actually do this stuff (not sure how it works right now)
    # aka like two servers and proxies? one on each side and whatnot/
    # this is client server
    server = XMLRPC::Client.new(ip, "/" , port)
    @main_server = server.proxy("server")

    # Thread.new { @server.serve()}
    # do other stuff
  end

  #---------------------------------------
  # RPC: NOTE PROB DONT NEED THIS SHIT ANY MORE
  #----------------------------------------

  # notes:
  # Called by server when opposing player has submitted their move. 
  # This allows client to know that the board has been updated in one of the games
  # Possible updates are:
  # Opposing player has won
  # Opposing player went - game still going on
  # Opposing player went and tied game
  # Opposing player quit
  # If the game_id matches the current game_id, 
  # the client should update the board and let the user know it is their turn
  # Perhaps a toast message, or flag/notification light somewhere will pop 
  # up letting the user know that a game has been updated

  def game_updated(game_id)
    # TODO implement
    puts ("#{game_id} has been updated do something!")
  end

end