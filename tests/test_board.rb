require File.dirname(__FILE__) + '/../model/board'

class TestBoard < Board

  # sets (i,j) to k
  def set(i, j, k)
    @grid[i][j] = k
  end

end
