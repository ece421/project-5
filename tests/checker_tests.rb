gem 'minitest'

require 'minitest/autorun'
require File.dirname(__FILE__) + '/../checker/checker'
require File.dirname(__FILE__) + '/../model/board'
require File.dirname(__FILE__) + '/../tests/test_board'
require File.dirname(__FILE__) + '/../model/game'

class CheckerTests < MiniTest::Test

  def setup
    @connect_4_board = TestBoard.new(6,6)
    @otto_board = TestBoard.new(6,6, Checker::PATTERN_OTTO_TOOT)
  end

  def test_empty_matrix
    assert_equal Checker.check(@connect_4_board), Game::GAME_NOT_OVER, "New connect 4 game was over"
    assert_equal Checker.check(@otto_board), Game::GAME_NOT_OVER, "New otto/toot game was over"
  end

  def test_simple_horizontal_win_connect_four
    board = TestBoard.new(4,4)
    board.add_piece(0,1)
    board.add_piece(1,1)
    board.add_piece(2,1)
    board.add_piece(3,1)

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Horizontal win didn't end game"
  end

  def test_simple_horizontal_win_otto_toot
    board = TestBoard.new(4,4, Checker::PATTERN_OTTO_TOOT)
    board.add_piece(0,1)
    board.add_piece(1,2)
    board.add_piece(2,2)
    board.add_piece(3,1)

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Horizontal win didn't end game"
  end

  def test_simple_vertical_win_connect_four
    board = TestBoard.new(4,4)
    board.add_piece(0,1)
    board.add_piece(0,1)
    board.add_piece(0,1)
    board.add_piece(0,1)

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Vertical win didn't end game"
  end

  def test_simple_vertical_win_otto_toot
    board = TestBoard.new(4,4,Checker::PATTERN_OTTO_TOOT)
    board.add_piece(0,1)
    board.add_piece(0,9)
    board.add_piece(0,9)
    board.add_piece(0,1)

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Vertical win didn't end game"
  end

  def test_simple_diagonal_left_win_connect_four
    board = TestBoard.new(4,4)
    board.add_piece(0,1)
    board.set(1,1,Piece.new(1))
    board.set(2,2,Piece.new(1))
    board.set(3,3,Piece.new(1))

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Diagonal win didn't end game"
  end

  def test_simple_diagonal_win_left_otto_toot
    board = TestBoard.new(4,4,Checker::PATTERN_OTTO_TOOT)
    board.add_piece(0,1)
    board.set(1,1,Piece.new(2))
    board.set(2,2,Piece.new(2))
    board.set(3,3,Piece.new(1))

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Diagonal win didn't end game"
  end

  def test_simple_diagonal_right_win_connect_four
    board = TestBoard.new(4,4)
    board.set(0,3,Piece.new(1))
    board.set(1,2,Piece.new(1))
    board.set(2,1,Piece.new(1))
    board.set(3,0,Piece.new(1))

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Diagonal win didn't end game"
  end

  def test_simple_diagonal_win_right_otto_toot
    board = TestBoard.new(4,4,Checker::PATTERN_OTTO_TOOT)
    board.set(0,3, Piece.new(1))
    board.set(1,2,Piece.new(2))
    board.set(2,1,Piece.new(2))
    board.set(3,0,Piece.new(1))

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Diagonal win didn't end game"
  end

  def test_multiple_win_connect_four
    board = TestBoard.new(4,4)
    for i in 0..3
      for j in 0..3
        board.set(i,j, Piece.new(1))
      end
    end

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Multiple wins weren't detected"
  end

  def test_multiple_win_otto_toot
    board = TestBoard.new(4,4,Checker::PATTERN_OTTO_TOOT)
    for i in 0..3
      for j in 0..3
        board.set(i,j, Piece.new(1))
      end
    end
    board.set(1,1, Piece.new(2))
    board.set(1,2, Piece.new(2))
    board.set(2,1, Piece.new(2))
    board.set(2,2, Piece.new(2))

    assert_equal Checker.check(board), Game::GAME_OVER_WIN, "Multiple wins weren't detected"
  end

  def test_three_isnt_four_connect_four
    board = TestBoard.new(4,4)
    board.set(0,0, Piece.new(1))
    board.set(1,1, Piece.new(1))
    board.set(2,2, Piece.new(1))
    board.set(3,3, Piece.new(2))

    assert_equal Checker.check(board), Game::GAME_NOT_OVER, "Game shouldn't be over"
  end

  def test_three_isnt_four_otto_toot
    board = TestBoard.new(4,4,Checker::PATTERN_OTTO_TOOT)
    board.set(0,0, Piece.new(1))
    board.set(1,1, Piece.new(2))
    board.set(2,2, Piece.new(2))
    board.set(3,3, Piece.new(2))

    assert_equal Checker.check(board), Game::GAME_NOT_OVER, "Game shouldn't be over"
  end

end