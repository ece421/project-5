require 'xmlrpc/client'
require_relative '../server/response'

def check_status(k)
  if k.status_code != 1
    puts "Wrong stuff: #{k}"
  end
end

ip_address = ENV['HOSTNAME']
if ip_address == nil
  ip_address = Socket.gethostname
end
puts "Using ip: #{ip_address}"

server = XMLRPC::Client.new(ip_address, '/', 50505)
prox = server.proxy('server')

user_1 = 1
user_1_type = 'r'
user_2_type ='b'
user_2 = 7

start_game_response = Response::decode(prox.start_game(user_1,user_2, 'b', 'r'))

if start_game_response.status_code == Response::STATUS_GAME_ALREADY_IN_PROGRESS
  puts "Game already in progress!"
elsif start_game_response.status_code != Response::STATUS_OK
  puts "Couldn't start game: #{start_game_response}"
  exit
end

current_game_response   = Response::decode(prox.get_current_games(user_1))
if current_game_response.status_code !=  Response::STATUS_OK
  puts "Puts couldn't get game list: #{current_game_response}"
  exit
end

puts "user_1 is currently playing #{current_game_response.body.size} games"

require_relative '../model/board'

current_game_response.body.each{ |game|
  game_id = game[0]
  if game[1] == user_1
    other_player_id = game[2]
  else
    other_player_id = game[2]
  end
  board = game[3]

  puts "Game: #{game_id}"

  puts "board looks like: "
  puts board.display_board

  username = Response::decode(prox.get_user_name(user_1)).body
  other_username = Response::decode(prox.get_user_name(user_2)).body

  my_turn_response = Response::decode(prox.is_it_my_turn(game_id, user_1))
  check_status(my_turn_response)

  if my_turn_response.body
    # Its my turn, lets make a move
    puts "Playing as #{user_1_type}, name: #{username}"
    make_move_response = Response::decode(prox.add_piece(game_id, 0, user_1_type, user_1))
  else
    # other persons turn
    puts "playing as #{user_2_type}, name: #{other_username}"
    make_move_response = Response::decode(prox.add_piece(game_id, 2, user_2_type, other_player_id))
  end
  check_status(make_move_response)

  new_board = Response::decode(prox.get_board(game_id))
  check_status(new_board)
  puts "new board looks like: "

  decoded_new_board = new_board.body
  puts decoded_new_board.display_board

}

leaderboard_response = Response::decode(prox.get_leaderboard)
check_status(leaderboard_response)
l_data = leaderboard_response.body

puts "leaderboard I guess: #{l_data}"

wins_response = Response::decode(prox.get_wins(user_1))
check_status(wins_response)
puts "user 1 has won: #{wins_response.body} games"
loss_response = Response::decode(prox.get_losses(user_1))
check_status(loss_response)
puts "user 1 has lost: #{loss_response.body} games"
