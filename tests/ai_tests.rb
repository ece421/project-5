gem 'minitest'

require 'minitest/autorun'
require File.dirname(__FILE__) + '/../model/player/ai_player'
require File.dirname(__FILE__) + '/../model/board'
require File.dirname(__FILE__) + '/../tests/test_board'
require File.dirname(__FILE__) + '/../model/piece'

class AITests < MiniTest::Test

  def setup
    @player = AIPlayer.new(1, 'Billy Mays', 4, Checker::PATTERN_CONNECT_FOUR, 2, AIPlayer::SMART)
  end

  def test_finish_him_same_column
    board = TestBoard.new(4,4)
    board.set(0,0,Piece.new(1))
    board.set(0,1,Piece.new(1))
    board.set(0,2,Piece.new(1))

    assert_equal @player.check_for_win(board), 0, "AI couldn't find winning column"
  end

  def test_finish_him_same_row
    board = TestBoard.new(4,4)
    board.set(0,0,Piece.new(1))
    board.set(1,0,Piece.new(1))
    board.set(3,0,Piece.new(1))

    assert_equal @player.check_for_win(board), 2, "AI couldn't find winning column"
  end

  def test_finish_him_diagonal
    board = TestBoard.new(4,4)
    board.set(1,1, Piece.new(1))
    board.set(2,2, Piece.new(1))
    board.set(3,3, Piece.new(1))

    assert_equal @player.check_for_win(board), 0, "AI couldn't find winning column"
  end

  def test_no_win_filled_vertical
    board = TestBoard.new(4,4)
    board.set(0,0,Piece.new(1))
    board.set(0,1,Piece.new(1))
    board.set(0,2,Piece.new(1))
    board.set(0,3,Piece.new(2))

    assert_equal @player.check_for_win(board), -1, "AI thought there was winning place"
  end

  def test_no_win_filled_horizontal
    board = TestBoard.new(4,4)
    board.set(0,0, Piece.new(1))
    board.set(1,0, Piece.new(1))
    board.set(2, 0, Piece.new(1))
    board.set(3, 0, Piece.new(2))

    assert_equal @player.check_for_win(board), -1, "AI Thought it could win"
  end

  def test_no_win_filled_diagonal
    board = TestBoard.new(4,4)
    board.set(0,0, Piece.new(1))
    board.set(1,1, Piece.new(1))
    board.set(2,2, Piece.new(1))
    board.set(3,3, Piece.new(2))

    assert_equal @player.check_for_win(board), -1, "AI thought they could win"
  end

  def test_no_opponent_win_empty
    board = TestBoard.new(4,4)
    expected = [0,1,2,3]
    assert_equal @player.check_for_opponent_win(board), expected, "AI thought opponent could win on empty board"
  end

  def test_opponent_win_horizontal_possibility
    board = TestBoard.new(4,4)
    expected = [0,1,2]
    board.add_piece(0,1)
    board.add_piece(1,2)
    board.add_piece(2,1)
    board.add_piece(0,2)
    board.add_piece(1,2)
    board.add_piece(2,2)

    assert_equal @player.check_for_opponent_win(board), expected, "AI couldn't find possible opponent win"
  end

  def test_opponent_win_vertical_possibility
    board = TestBoard.new(4,4)
    expected = [0]
    board.add_piece(0,2)
    board.add_piece(0,2)
    board.add_piece(0,2)

    assert_equal @player.check_for_opponent_win(board), expected, "AI couldn't find the possible opponent win"
  end

  def test_opponent_win_double_dip_vertical
    board = TestBoard.new(4,4)
    expected = []
    board.add_piece(0,2)
    board.add_piece(0,2)
    board.add_piece(0,2)
    board.add_piece(3,2)
    board.add_piece(3,2)
    board.add_piece(3,2)

    assert_equal @player.check_for_opponent_win(board), expected, "AI found somewhere to go"
  end

  def test_opponent_win_diagonal
    board = TestBoard.new(4,4)
    expected = [0,2]
    board.add_piece(0,2)
    board.add_piece(2,1)
    board.add_piece(2,2)
    board.add_piece(2,2)
    board.add_piece(3,1)
    board.add_piece(3,2)
    board.add_piece(3,2)
    board.add_piece(3,2)

    assert_equal @player.check_for_opponent_win(board), expected, "AI couldn't find the hole"
  end

  def test_full_columns_horizontal_opponent_win
    board = TestBoard.new(6,6)
    board.add_piece(0,2)
    board.add_piece(0,2)
    board.add_piece(0,2)
    board.add_piece(0,1)
    board.add_piece(0,1)
    board.add_piece(0,2)

    board.add_piece(1,1)

    board.add_piece(2,1)
    board.add_piece(2,2)
    board.add_piece(2,2)
    board.add_piece(2,1)
    board.add_piece(2,2)
    board.add_piece(2,2)

    board.add_piece(3,2)
    board.add_piece(3,1)
    board.add_piece(3,2)
    board.add_piece(3,1)
    board.add_piece(3,2)
    board.add_piece(3,2)

    board.add_piece(4,1)
    board.add_piece(4,1)
    board.add_piece(4,1)
    board.add_piece(4,2)
    board.add_piece(4,1)
    board.add_piece(4,1)

    expected = [5]

    assert_equal @player.check_for_opponent_win(board), expected, "AI died"

  end

end