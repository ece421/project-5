require 'xmlrpc/client'

require_relative 'server_database'
require_relative '../model/board'
require_relative '../checker/checker'
require_relative '../contracts/server_manager_contracts'

class ServerManager

  include BoardContracts

  BOARD_ROWS = 6
  BOARD_COLS = 7

  def initialize
    @games = {}
    @users = {}
    @results = {}
    @db_manager = ServerDatabase.get_instance
  end

  # Tries to start a game against the opposing player
  # Server will reply with: game already_exists + board of game,
  # User doesn't exist
  # blank board signifying start of game
  # server doesn't let other player know of game until it is their turn and they call
  # get_current_games
  def start_game(opposing_user_id, user_id, user_1_token, user_2_token, game_type=0)

    puts "Starting game with type: #{game_type}"
    board = Board.new(BOARD_ROWS, BOARD_COLS, get_game_pattern(game_type))
    result = create_game(user_id, opposing_user_id, board, user_1_token, user_2_token)

    return result.encode
  end

  # Returns the current player
  def get_current_player(game_id)
    @db_manager.get_current_player(game_id).encode
  end

  # returns the current type for the given game_id
  def get_current_type(game_id)
    @db_manager.get_current_type(game_id).encode
  end

  # Adds piece in column to game
  # Returns response to server: acceptance of piece, game_is_already_over
  # its_not_your_turn, column_full
  def add_piece(game_id, col, player_type, user_id)
    return @db_manager.add_piece(game_id, col, player_type, user_id).encode
  end

  # Returns the game board of a certain game
  # possible errors: Game doesn't exist, client is player in this game
  def get_board(game_id)
    return @db_manager.get_board(game_id).encode
  end

  # Returns if it is this players turn or not in the game
  def is_it_my_turn(game_id, user_id)
    return @db_manager.is_it_my_turn?(game_id, user_id).encode
  end

  # Returns a leaderboard table
  # Blank table if nothing there
  def get_leaderboard
    result = @db_manager.get_leaderboard
    return result.encode
  end

  # Returns a list of the players current games
  # List element: [game_id, current_player, other_player, board, finished]
  def get_current_games(user_id)
    return @db_manager.get_current_games(user_id).encode
  end

  # Starts a random game, puts the player in the random players pool
  # Puts client in list of users who also want to start a random game
  # List is FIFO, but client must not already be in game with other chosen player
  # Server doesn't have to do anything but update that clients list of unfinshed
  # games when a match is found
  def start_random_game(user_id, game_type='connect_4')
    puts "Starting a random game"
    return 'ya jk'
  end

  # creates a new user with this name, returning their id
  def create_user(name)
    result = @db_manager.create_user(name)
    result.encode
  end

  # Returns pattern of game chosen by user
  def get_game_pattern(p)
    if p == 0
      return Checker::PATTERN_CONNECT_FOUR
    else
      return Checker::PATTERN_OTTO_TOOT
    end
  end

  # returns the username for the user_id
  def get_user_name(id)
    @db_manager.get_user_name(id).encode
  end

  # Returns the user id for the user or -1 if no user exists
  def get_user_id(name)
    @db_manager.user_id(name).encode
  end

  # returns wins
  def get_wins(user_id)
    @db_manager.get_wins(user_id).encode
  end

  # return losses
  def get_losses(user_id)
    @db_manager.get_losses(user_id).encode
  end

  private
  # Creates the new game and puts it in the database
  def create_game(user_id, opposing_user_id, board, user_1_token, user_2_token)
    @db_manager.create_game(user_id, opposing_user_id, board, user_1_token, user_2_token)
  end

  ##########################################################
  # Test methods - probably shouldn't let users call these #
  ##########################################################

  # test method for db.end_game
  def end_game(game_id)
    @db_manager.end_game(game_id).encode
  end

  # test method for db.next_turn
  def next_turn(game_id)
    @db_manager.next_turn(game_id).encode
  end

end