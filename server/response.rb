require 'xmlrpc/client'
require_relative '../model/board'

# Response class that wraps all responses from server
class Response

  attr_reader :status_code, :body

  # Creates a Response object with the provided status_code and body
  def initialize(status_code, body)
    @status_code = status_code
    @body = body
  end

  # Returns Base64 encoded Marshal dumped string version of itself
  def encode
    XMLRPC::Base64.encode(Marshal.dump(self))
  end

  # returns Response object corresponding to Base64 encoded Marshal dump of itself
  def self.decode(silly_string)
    Marshal.load(XMLRPC::Base64.decode(silly_string))
  end

  # Represent this as a string
  def to_s
    str = "Status code: #{@status_code}\n"
    str += "Body: #{body}"
    str
  end

  # status code constants - expecting result
  # Returned when everything happened correctly
  STATUS_OK = 1
  # Client tried to operate on a user who does not exist
  STATUS_USER_DOES_NOT_EXIST = -1
  # Client tried to operate on a game the user was not in
  STATUS_USER_NOT_IN_GAME = -2
  # Client tried to perform action when it wasn't their turn
  STATUS_NOT_USER_TURN = -3
  # Client tried to operate on a game that doesn't exist
  STATUS_GAME_DOES_NOT_EXIST = -4
  # Client tried to start a game between two users who already have a game in progress
  STATUS_GAME_ALREADY_IN_PROGRESS = -5
  # Client tried to start a game with themselves
  STATUS_DONT_PLAY_WITH_YOURSELF = -6
  # Client tried to put a piece in an already full column
  STATUS_COLUMN_ALREADY_FULL = -7
  # Client tried to perform operation on already finished game
  STATUS_GAME_ALREADY_OVER = -8
  # Client tried to create a user who already exists
  STATUS_USER_ALREADY_EXISTS = -9
  # Client tried to start game with same types
  STATUS_TYPES_ARE_IDENTICAL = -10

end