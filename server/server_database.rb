require 'mysql'

require_relative '../checker/checker'
require_relative '../model/game'
require_relative 'response'
require_relative '../contracts/server_database_contracts'

# Master server database class, providing all interactions with the database
class ServerDatabase

  include ServerDatabaseContracts
  #@@HOST = 'localhost'
  #@@USER = 'bob'
 # @@DB = 'ece421grp5'
 # @@PORT = 13010
 # @@PASS = 'bob_pass'


  # TODO: put this in a config file somewhere - also change for lab computers
  @@HOST = 'mysqlsrv.ece.ualberta.ca'
  @@USER = 'ece421grp5'
  @@DB = 'ece421grp5'
  @@PORT = 13010
  @@PASS = 'd30ISfIp'

  def initialize(db_connection)
    @dbh = db_connection
  end

  private_class_method :new

  # Returns a new instance of the database object - required for each separate thread
  def ServerDatabase.get_instance
    result = nil
    begin
      dbh = Mysql.real_connect(@@HOST, @@USER, @@PASS, @@DB, @@PORT)
      result = new(dbh)
    rescue Mysql::Error => e
        puts "error code: #{e.errno}"
        puts "error message: #{e.error}"
        puts "sqlstate: #{e.sqlstate}" if e.respond_to?('sqlstate')
        puts 'Error connecting to database'
    end
    result
  end

  # Closes connection to db
  def close
    @dbh.close
  end

  # returns a player id for a given name
  def user_id(name)
    user_id = user_exists?(name)
    if user_id == -1
      return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_id)
    end
    Response.new(Response::STATUS_OK, user_id)
  end

  # creates a new game, returns the game id
  def create_game(user_1, user_2, board, type_1, type_2)
    return Response.new(Response::STATUS_DONT_PLAY_WITH_YOURSELF, user_1) if user_1 == user_2
    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_1) if -1 == user_exists_name?(user_1)
    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_1) if -1 == user_exists_name?(user_2)

    return Response.new(Response::STATUS_TYPES_ARE_IDENTICAL, type_1) if type_1 == type_2

    return Response.new(Response::STATUS_GAME_ALREADY_IN_PROGRESS, user_1) if users_playing?(user_1, user_2)

    sql = 'insert into games (current_player, other_player, board, finished, created, current_type, other_type) '
    sql += 'values(?, ?, ?, false, now(), ?, ?)'

    prep_stat = @dbh.prepare(sql)
    stmt = prep_stat.execute(user_1, user_2, Marshal.dump(board), type_1, type_2)

    result = stmt.insert_id
    stmt.close

    return Response.new(Response::STATUS_OK, result)
  end

  # Adds a piece to the specified game
  def add_piece(game_id, col, piece, user_id)
    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_id) if user_exists_name?(user_id) == -1
    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if !game_exists?(game_id)
    return Response.new(Response::STATUS_USER_NOT_IN_GAME, user_id) if !player_in_game?(game_id, user_id)
    return Response.new(Response::STATUS_NOT_USER_TURN, user_id) if !is_it_my_turn?(game_id, user_id)

    old_board = Marshal.load(get_game_board(game_id))

    return Response.new(Response::STATUS_COLUMN_ALREADY_FULL, col) if old_board.column_full?(col)
    game_status = game_over?(game_id)
    if game_status == -1
      return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id)
    elsif game_status == 1
      return Response.new(Response::STATUS_GAME_ALREADY_OVER, game_id)
    end

    old_board.add_piece(col, piece)

    update_board(old_board, game_id)

    # check if user won game or tied game
    status = Checker::check(old_board)
    if status == Game::GAME_NOT_OVER
      next_turn(game_id)
      return Response.new(Response::STATUS_OK, old_board)
    else
      return end_game(game_id)
    end
  end

  # returns the current users type
  def get_current_type(game_id)
    result = get_current_user_type(game_id)

    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if result == -1
    return Response.new(Response::STATUS_OK, result[0])
  end

  # returns the other users type
  def get_other_type(game_id)
    result = get_other_user_type(game_id)

    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if result == -1
    return Response.new(Response::STATUS_OK, result[0])
  end

  # Sets the game to finished
  def end_game(game_id)
    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if !game_exists?(game_id)

    sql = 'update games set finished=1 where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)

    stmt.close()

    board = Marshal.load(get_game_board(game_id))

    # TODO: Check if board is -1 or something
    game_status = Checker::check(board)

    tie_game = 1

    winner = get_current_user(game_id)
    loser = get_other_player(game_id)

    # TODO: do we do anything else if game is not over?

    if game_status == Game::GAME_OVER_WIN
      tie_game = 0
    elsif game_status == Game::GAME_OVER_QUIT
      # If player quit, then other player is real winner
      tie_game = 0
      temp = winner
      winner = loser
      loser = temp
    end

    add_results(game_id, tie_game, winner, loser)

    Response.new(Response::STATUS_OK, game_status)
  end

  # returns a list of current games for this user
  def get_current_games(user_id)
    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_id) if user_exists_name?(user_id) == -1

    games =  get_games(user_id, 0)
    Response.new(Response::STATUS_OK, games)
  end

  # returns leaderboard
  def get_leaderboard
    sql = 'select * from users'
    leaderboard = []

    prep = @dbh.prepare(sql)
    stmt = prep.execute

    rows = stmt.num_rows

    rows.times {
      row = stmt.fetch

      id = row[0]
      name = row[1]
      wins = get_user_wins(id)[0]
      losses = get_user_losses(id)[0]
      ties = get_user_ties(id)[0]
      entry = [name, wins, losses, ties]
      leaderboard << entry
    }

    Response.new(Response::STATUS_OK, leaderboard)
  end

  # creates a new user returning their id, or -1 if username already exists
  def create_user(name)
    sql = 'insert into users (name) values(?)'

    if user_exists?(name) != -1
      return Response.new(Response::STATUS_USER_ALREADY_EXISTS, name)
    end

    prep_stat = @dbh.prepare(sql)
    stmt = prep_stat.execute(name)

    result = stmt.insert_id
    stmt.close

    return Response.new(Response::STATUS_OK, result)
  end

  # returns if it is a users turn or not for a game
  def is_it_my_turn?(game_id, user_id)
    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if !game_exists?(game_id)
    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_id) if !user_exists_name?(user_id)
    return Response.new(Response::STATUS_USER_NOT_IN_GAME, user_id) if !player_in_game?(game_id, user_id)
    return Response.new(Response::STATUS_GAME_ALREADY_OVER, game_id) if game_over?(game_id) == 1

    current_player = get_current_user(game_id)
    Response.new(Response::STATUS_OK, current_player == user_id)
  end

  # returns the board for a given game
  def get_board(game_id)

    result = get_game_board(game_id)

    if result == -1
      return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id)
    end

    real_board = Marshal.load(result)

    # TODO: Should probably handle this nicer
    Response.new(Response::STATUS_OK, real_board)
  end

  # returns if player is in game
  def player_in_game?(game_id, user_id)
    return user_in_game?(game_id, user_id)
  end

  # Returns number of wins for a player
  def get_wins(player_id)

    result = get_user_wins(player_id)

    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, player_id) if result == nil
    return Response.new(Response::STATUS_OK, result[0])
  end

  # Returns the number of losses for a player
  def get_losses(player_id)
    # returns leaderboard
    sql = 'select count(*) from game_results where loser=? AND tie_game=0'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(player_id)
    result = stmt.fetch

    return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, player_id) if result == nil
    return Response.new(Response::STATUS_OK, result[0])
  end

# TODO: Make this method private again
  # Changes the current player_id to the other player
  def next_turn(game_id)
    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if !game_exists?(game_id)

    old_current_player = get_current_user(game_id)
    old_other_player = get_other_player(game_id)
    old_current_type = get_current_user_type(game_id)[0]
    old_other_type = get_other_user_type(game_id)[0]

    sql = 'update games set current_player=? where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(old_other_player, game_id)
    stmt.close

    sql2 = 'update games set other_player=? where game_id=?'
    prep = @dbh.prepare(sql2)
    stmt = prep.execute(old_current_player, game_id)
    stmt.close

    sql3 = 'update games set current_type=? where game_id=?'
    prep = @dbh.prepare(sql3)
    stmt = prep.execute(old_other_type, game_id)
    stmt.close

    sql4 = 'update games set other_type=? where game_id=?'
    prep = @dbh.prepare(sql4)
    stmt = prep.execute(old_current_type, game_id)
    stmt.close

    return Response.new(Response::STATUS_OK, nil)

  end

  # returns user_id
  def get_user_id(username)
    result = user_exists?(username)
    if result == -1
      return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, username)
    end
    return Response.new(Response::STATUS_OK, result)
  end

  # returns username
  def get_user_name(user_id)
    result = user_exists_name?(user_id)
    if result == -1
      return Response.new(Response::STATUS_USER_DOES_NOT_EXIST, user_id)
    end
    return Response.new(Response::STATUS_OK, result)
  end

  # returns those pesky current players
  def get_current_player(game_id)
    result = get_current_user(game_id)
    return Response.new(Response::STATUS_GAME_DOES_NOT_EXIST, game_id) if result == -1
    Response.new(Response::STATUS_OK, result)
  end

  # private functions - don't return responses
  private

  # returns the current player
  def get_current_user(game_id)
    # select current_player from games where game_id='game_id';
    sql = 'select current_player from games where (game_id=?)'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return -1 if result == nil

    result[0]
  end

  # returns other user type
  def get_other_user_type(game_id)
    sql = 'select other_type from games where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)

    result = stmt.fetch
    stmt.close
    result
  end

  # returns current user type
  def get_current_user_type(game_id)
    sql = 'select current_type from games where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)

    result = stmt.fetch
    stmt.close
    result
  end

  # returns number of user wins
  def get_user_wins(id)
    sql = 'select count(*) from game_results where winner=? AND tie_game=0'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(id)
    stmt.fetch
  end

  # returns number of user losses
  def get_user_losses(id)
    sql = 'select count(*) from game_results where loser=? AND tie_game=0'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(id)
    stmt.fetch
  end

  # returns number of user ties
  def get_user_ties(id)
    sql = 'select count(*) from game_results where (winner=? or loser=?)AND tie_game=1'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(id, id)
    stmt.fetch
  end

  # returns if user is in game
  def user_in_game?(game_id, user_id)
    sql = 'select game_id from games where ((current_player=? or other_player=?)'
    sql += 'and game_id=?)'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(user_id, user_id, game_id)
    result = stmt.fetch
    stmt.close

    return result != nil
  end

  # returns the game board id
  def get_game_board(game_id)
    sql = 'select board from games where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return -1 if result == nil
    result[0]
  end

  # returns list of games for user, if current is true then just games that aren't finished
  def get_games(user_id, finished=1)
    sql = 'select game_id, current_player, other_player, board, finished from games '
    sql += 'where ((current_player=? or other_player=?) and finished=?)'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(user_id, user_id, finished)

    rows = stmt.num_rows

    games = []

    rows.times(){
      row = stmt.fetch
      row[3] = Marshal.load(row[3])
      games << row
    }

    stmt.close

    games
  end

  # returns other player
  def get_other_player(game_id)
    # select other_player from games where game_id='game_id';
    sql = 'select other_player from games where (game_id=?)'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return -1 if result == nil

    result[0]
  end

  # updates the valid board
  def update_board(new_board, game_id)

    # TODO: Respond with Respond object
    return -1 if !game_exists?(game_id)

    sql = 'update games set board=? where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(Marshal.dump(new_board), game_id)

    stmt.close()

    # TODO: Return some shit here
  end

  # returns true if users are currently playing a game
  def users_playing?(user_1, user_2)
    sql = 'select game_id from games where (((current_player=? and other_player=?) OR '
    sql += '(current_player=? and other_player=?)) AND finished=0)'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(user_1, user_2, user_2, user_1)
    result = stmt.fetch
    stmt.close

    return result != nil
  end

  # returns if game exists
  def game_exists?(game_id)
    # select TRUE from games where game_id=game_id;
    sql = 'select * from games where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return result != nil
  end

  # returns if unfinished game exists
  def unfinished_game_exists(game_id)
    # select TRUE from games where game_id=game_id;
    sql = 'select * from games where game_id=? and finished=0'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return result != nil
  end

  # Returns if game is over or not
  def game_over?(game_id)
    sql = 'select finished from games where game_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(game_id)
    result = stmt.fetch
    stmt.close

    return -1 if result == nil

    result[0]
  end

  # returns -1 if user doesn't exist else their id
  def user_exists?(name)
    # select user_id from users where name=name
    sql = 'select user_id from users where name=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(name)
    result = stmt.fetch
    stmt.close

    if result == nil
      return -1
    end
    result[0]
  end

  # returns -1 is user doesn't exist else their name
  def user_exists_name?(id)
    sql = 'select name from users where user_id=?'
    prep = @dbh.prepare(sql)
    stmt = prep.execute(id)
    result = stmt.fetch
    stmt.close

    if result == nil
      return -1
    end
    result[0]
  end

  # Used to add to game_results once game is over
  # TODO: Or to make simpler, could just be passed a game_id and hope were only passed this
  # completion of game, then winner would be current player, user other player, and we can figure
  # out if it won or not
  def add_results(game_id, tie_game, winner, loser)
    # insert into game_results (game_id, tie_game, winner, loser) values(1, FALSE, 1, 3);
    # TODO: Use response objects

    return -1 if !game_exists?(game_id)

    sql = 'insert into game_results (game_id, tie_game, winner, loser) '
    sql += 'values(?, ?, ?, ?)'

    prep_stat = @dbh.prepare(sql)
    stmt = prep_stat.execute(game_id, tie_game, winner, loser)

    stmt.close
  end

end
