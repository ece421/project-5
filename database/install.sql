# call via: source ./database/install.sql
use ece421grp5;
# Must drop tables in this order because of foreign key constraints
DROP TABLE IF EXISTS game_results;
DROP TABLE IF EXISTS games;
DROP TABLE IF EXISTS users;

CREATE TABLE `users`(
  `user_id` INT(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL UNIQUE,

  PRIMARY KEY(user_id)
);

CREATE TABLE `games`(
  `game_id` INT (10) NOT NULL AUTO_INCREMENT,
  `current_player` INT(10) NOT NULL,
  `other_player` INT(10) NOT NULL,
  `board` blob NOT NULL,
  `finished` BOOLEAN NOT NULL DEFAULT FALSE,
  `created` DATETIME NOT NULL,
  `current_type` VARchar(1) NOT NULL,
  `other_type` VARCHAR(1) NOT NULL,

  PRIMARY KEY(`game_id`),
  FOREIGN KEY (`other_player`) REFERENCES users(user_id),
  FOREIGN KEY (`current_player`) REFERENCES users(user_id)
);
# commented out to work on lab machines
#ENGINE=InnoDB;

CREATE TABLE `game_results`(
  `game_id` INT(10) NOT NULL,
  `tie_game` BOOLEAN NOT NULL,
  `winner` INT(10) NOT NULL,
  `loser` INT(10) NOT NULL,

  FOREIGN KEY (`game_id`) REFERENCES games(game_id),
  FOREIGN KEY (`winner`) REFERENCES users(user_id),
  FOREIGN KEY (`loser`) REFERENCES users(user_id)

);