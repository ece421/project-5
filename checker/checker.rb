require File.dirname(__FILE__) + '/../contracts/checker_contracts'
require File.dirname(__FILE__) + '/../model/board'
require File.dirname(__FILE__) + '/../model/game'

# Module used to check if a winning pattern is found
module Checker

  class << self
    include CheckerContracts
  end

  PATTERN_OTTO_TOOT = /(\w)(?!\1)(\w){2}\1/
  PATTERN_OTTO_2 = /(.)(.)\2\1/ 
  PATTERN_CONNECT_FOUR = /(\w)\1{3}/ # match any character 4 times in a row

  # Checks if pattern exists in board horizontally, vertically or diagonally
  # returns respective status of board constant
  def Checker.check(board)
    pattern = board.pattern
    check_preconditions(board, pattern)

    if horizontal_win?(board, pattern) || vertical_win?(board, pattern) ||
        diagonal_win?(board, pattern)
      return Game::GAME_OVER_WIN
    end

    if board_full?(board)
      return Game::GAME_OVER_TIE
    end
    # if we got here, game isn't over yet
    Game::GAME_NOT_OVER
  end

  # Returns true if pattern is found horizontally (in a row)
  def Checker.horizontal_win?(board, pattern)
    horizontal_win_preconditions(board, pattern)
    board.grid.transpose.each do |row|
      if row.join =~ pattern 
        if pattern ==  PATTERN_OTTO_TOOT 
          if row.join =~ PATTERN_OTTO_2 
            return true
          end
        else
          return true
        end
      end
    end
    false
  end


  # Returns true if pattern is found vertically (in a column)
  def Checker.vertical_win?(board, pattern)
    vertical_win_preconditions(board, pattern)
    board.grid.each do |col|
      if col.join =~ pattern 
         if pattern ==  PATTERN_OTTO_TOOT 
          if col.join =~ PATTERN_OTTO_2 
            return true
          end
        else
          return true
        end
      end
    end
    false
  end

  # Returns true if pattern is found in a diagonal
  def Checker.diagonal_win?(board, pattern)
    diagonal_win_preconditions(board, pattern)
    # TODO: might have to check this again
    board.get_diagonals.each do |diag|
      if diag.join =~ pattern 
         if pattern ==  PATTERN_OTTO_TOOT 
          if diag.join =~ PATTERN_OTTO_2 
            return true
          end
        else
          return true
        end
      end
    end
    false
  end

  # Returns true if board is full
  def Checker.board_full?(board)
    board_full_preconditions(board)
    board.full?
  end

end