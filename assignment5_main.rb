require './model/client.rb'
require './view/view_client_launcher'
require './controller/client_controller'

# To install DB, enter a mysql shell
# source database/mysql;

# to run server, first run ruby ./assignment5_server.rb in another terminal instance
# then run this file

# prob want to make a client first

# TA: you may want to change this ip_address
# also assignment5_server.rb must be ran first

# Last project!
# Group 5: Chris Hut, Vincent Phung


ip_address = ENV['HOSTNAME']
if ip_address == nil
 ip_address = Socket.gethostname
end
port = 13010
myClient = Client.new(ip_address,port)
controller = ClientController.new
controller.set_client(myClient)
view = ClientLauncher.new(controller)
