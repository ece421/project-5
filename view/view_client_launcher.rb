require 'gtk2'
require File.dirname(__FILE__) + '/../model/board_state'
require File.dirname(__FILE__) + '/../model/piece'
require_relative '../contracts/view_launcher_contracts'
require_relative '../utils'

class ClientLauncher

  include ViewLauncherContracts
  include Utils

  def initialize(controller)
    initialize_preconditions(controller)
    Gtk.init
    @builder = Gtk::Builder::new
    @builder.add_from_file(File.expand_path("../project5_client.glade", File.dirname(__FILE__)))
    #@builder.connect_signals{ |handler| method(handler) }  # (No handlers yet, but I will have eventually)
    setController(controller)
    @controller.set_builder(@builder)
    controller.set_view(self)
    controller.set_signal_handlers()
    # window = @builder.get_object("window_launcher")
    # window.signal_connect( "destroy" ) { Gtk.main_quit }
    # window.show()
    window = @builder.get_object("window_login")
    window.signal_connect("destroy"){Gtk.main_quit}
    window.show()
    Gtk.main()

    class_invariant
  end

  def setController(controller)
    @controller = controller
  end

  def getController(controller)
    @controller
  end

  def gtk_main_quit
    Gtk.main_quit()
  end

  def update(boardState)
    update_preconditions(boardState)
    class_invariant
    board = boardState.board
    currentPlayer = boardState.currentPlayer
    label = @builder.get_object("label_currentStatus")
    label.set_text(currentPlayer.name.to_s + "'s turn")
    board.grid.each_with_index {|col, colNum|
      col.each_with_index{ |token, row|
        if token.type!=Piece::EMPTY
          updateBoardImages(row,colNum,token,board.grid.size)
        end
      }
    }
    class_invariant
  end

  def updateBoard(board)
    # currentPlayer = boardState.currentPlayer
    # label = @builder.get_object("label_currentStatus")
    # label.set_text(currentPlayer.name.to_s + "'s turn")
    board.grid.each_with_index {|col, colNum|
      col.each_with_index{ |token, row|
        if token.type!=Piece::EMPTY
          updateBoardImages(row,colNum,token,board.grid.size)
        end
      }
    }
    class_invariant
  end


  def updateBoardImages(row,col,token,boardSize)
    class_invariant
    update_board_images_preconditions(row, col, token, boardSize)
    path =  File.expand_path("../resources", File.dirname(__FILE__))
    imageNum = (col + row*boardSize)
    tokenImage = @builder.get_object(imageNum.to_s)
    tokenImageName = getImageName(token.type)
    tokenImage.set_file(path.to_s+tokenImageName)
    class_invariant
  end

end